module.exports = [
    {
        id: 1,
        title: 'Hello World #1',
        content: 'Lorem Ipsum #1',
        authorId: 1,
        createdAt: Date.now(),
        updatedAt: Date.now(),
    },
    {
        id: 2,
        title: 'Hello World #2',
        content: 'Lorem Ipsum #2',
        authorId: 2,
        createdAt: Date.now(),
        updatedAt: Date.now(),
    },
    {
        id: 3,
        title: 'Hello World #3',
        content: 'Lorem Ipsum #3',
        authorId: 2,
        createdAt: Date.now(),
        updatedAt: Date.now(),
    },
    {
        id: 4,
        title: 'Hello World #4',
        content: 'Lorem Ipsum #4',
        authorId: 1,
        createdAt: Date.now(),
        updatedAt: Date.now(),
    },
]
