const { verify } = require('jsonwebtoken')
const { users } = require('../data')

module.exports = (authorization) => {
    if (!authorization) return null

    const token = authorization.split(' ')[1]

    if (!token) return null

    const decoded = verify(token, process.env.SECRET)

    if (!decoded) return null

    const user = users.find((user) => user.id === decoded.id)

    if (!user) return null

    return user
}
