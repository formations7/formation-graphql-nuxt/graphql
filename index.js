require('dotenv').config()
const { ApolloServer, PubSub } = require('apollo-server')
const schema = require('./graphql')
const { authMiddleware } = require('./middlewares')

const pubSub = new PubSub()

const server = new ApolloServer({
    schema,
    context: ({ req, connection }) => {
        const user = authMiddleware(
            connection
                ? connection.context.Authorization
                : req.headers.authorization,
        )

        return { user, pubSub }
    },
})

server.listen(3000).then(({ url, subscriptionsUrl }) => {
    console.log(`🚀  Server ready at ${url}`)
    console.log(`🚀 Subscriptions ready at ${subscriptionsUrl}`)
})
