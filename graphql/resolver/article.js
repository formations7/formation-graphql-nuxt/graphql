const { users } = require('../../data')

module.exports = {
    author: ({ authorId }, args, ctx) => {
        return users.find((user) => authorId === user.id)
    },
}
