const Query = require('./query')
const Mutation = require('./mutation')
const Subscription = require('./subscription')
const Article = require('./Article')
const User = require('./user')

module.exports = {
    Query,
    Mutation,
    Subscription,
    Article,
    User,
}
