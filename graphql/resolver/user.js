const { articles } = require('../../data')

module.exports = {
    articles: ({ id }, _, __) => {
        return articles.filter((article) => article.authorId === id)
    },
}
