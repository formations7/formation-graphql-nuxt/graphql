const { ApolloError } = require('apollo-server')
const { sign } = require('jsonwebtoken')
const { users, articles } = require('../../data')
const { ARTICLE_CREATED } = require('../constants')

module.exports = {
    createUser: (_, { data }, ctx) => {
        const user = {
            ...data,
            id: users.length + 1,
            createdAt: Date.now(),
            updatedAt: Date.now(),
        }

        users.push(user)
        return { token: sign({ id: user.id }, process.env.SECRET), user }
    },
    login: (_, { data }, ctx) => {
        const user = users.find(
            (user) =>
                user.email === data.email && user.password === data.password,
        )

        if (!user) {
            throw new ApolloError('Invalid email or password')
        }

        return {
            token: sign({ id: user.id }, process.env.SECRET),
            user,
        }
    },
    createArticle: (_, { data }, { user, pubSub }) => {
        if (!user) throw new ApolloError('Unauthanticated')

        const article = {
            id: articles.length + 1,
            title: data.title,
            content: data.content,
            authorId: user.id,
            createdAt: Date.now(),
            updatedAt: Date.now(),
        }

        pubSub.publish(ARTICLE_CREATED, { articleCreated: article })

        articles.push(article)

        return article
    },
    updateArticle: (_, { where, data }, { user }) => {
        if (!user) throw new ApolloError('Unauthanticated')

        const i = articles.map(({ id }) => id).indexOf(where.id)
        if (i === -1) {
            throw ApolloError('The id is invalid')
        }

        if (articles[i].authorId !== user.id)
            new ApolloError('What are you trying to do ?')

        articles[i] = {
            ...articles[i],
            ...data,
            updatedAt: Date.now(),
        }

        return articles
    },
    deleteArticle: (_, { where }, { user }) => {
        if (!user) throw new ApolloError('Unauthanticated')

        if (!articles[where.id]) {
            throw ApolloError('The id is invalid')
        }

        if (articles[where.id].authorId !== user.id)
            new ApolloError('What are you trying to do ?')

        articles.splice(articles.map(({ id }) => id).indexOf(where.id), 1)

        return articles
    },
}
