const { ApolloError } = require('apollo-server')
const { users, articles } = require('../../data')

module.exports = {
    me: (_, __, { user }) => {
        if (!user) throw new ApolloError('Unauthanticated request')

        return user
    },
    articles: (_, args, __) => {
        return articles
    },
    article: (_, { where }, ctx) =>
        articles.find((article) => article.id === where.id),
}
