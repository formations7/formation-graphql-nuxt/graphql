const { ARTICLE_CREATED } = require('../constants')

module.exports = {
    articleCreated: {
        subscribe: (_, __, { pubSub }) =>
            pubSub.asyncIterator([ARTICLE_CREATED]),
    },
}
