const { makeExecutableSchema } = require('apollo-server')

const typeDefs = require('./schema')
const resolvers = require('./resolver')

module.exports = makeExecutableSchema({
    typeDefs,
    resolvers,
})
