const { gql } = require('apollo-server')

module.exports = gql`
    input WhereUserInput {
        id: Int
        email: String
    }

    input UserLoginInput {
        email: String!
        password: String!
    }

    input CreateUserInput {
        name: String!
        email: String!
        password: String!
    }

    type User {
        id: Int!
        name: String!
        email: String!
        articles: [Article]!
        createdAt: DateTime!
        updatedAt: DateTime!
    }

    type AuthPayload {
        token: String!
        user: User!
    }
`
