const { gql } = require('apollo-server')

module.exports = gql`
    type Mutation {
        createUser(data: CreateUserInput!): AuthPayload!
        login(data: UserLoginInput): AuthPayload!
        createArticle(data: CreateArticleInput!): Article!
        updateArticle(
            where: WhereArticleInput!
            data: UpdateArticleInput!
        ): Article!
        deleteArticle(where: WhereArticleInput!): Article!
    }
`
