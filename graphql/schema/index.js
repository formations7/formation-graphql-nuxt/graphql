const { makeExecutableSchema } = require('apollo-server')

const Scalars = require('./scalars')
const Article = require('./article')
const User = require('./user')
const Query = require('./query')
const Mutation = require('./mutation')
const Subscritpion = require('./subscription')

module.exports = [Scalars, User, Article, Query, Mutation, Subscritpion]
