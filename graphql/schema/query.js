const { gql } = require('apollo-server')

module.exports = gql`
    type Query {
        me: User!
        articles: [Article]!
        article(where: WhereArticleInput!): Article
    }
`
