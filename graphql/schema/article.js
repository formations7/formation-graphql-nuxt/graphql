const { gql } = require('apollo-server')

module.exports = gql`
    input WhereArticleInput {
        id: Int!
    }

    input CreateArticleInput {
        title: String!
        content: String!
    }

    input UpdateArticleInput {
        title: String
        content: String
    }

    type Article {
        id: Int!
        title: String!
        content: String!
        author: User!
        updatedAt: DateTime!
        createdAt: DateTime!
    }
`
